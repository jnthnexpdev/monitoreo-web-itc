import { describe, it } from 'mocha';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

describe('Pruebas del middleware de autentificacion', () => {
    it('Verificar la firma del token', async () => {
        const tokenIncorrect = jwt.sign({ user: 'Administrador' }, 'ResidenciasItc2024');

        // Intenta verificar el token utilizando la clave secreta correcta
        try {
            jwt.verify(tokenIncorrect, process.env.SECRET);
            console.log('El token es válido y no ha sido modificado.');
            return;
        } catch (error) {
            console.error('El token es inválido o ha sido modificado.');
            return;
        }
    });
});