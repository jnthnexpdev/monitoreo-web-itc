import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { AlertsService } from '../../../shared/services/alerts/alerts.service';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/services/auth/auth.service';
import { CommonModule, NgClass } from '@angular/common';

@Component({
  selector: 'app-import-teachers',
  standalone: true,
  imports: [NgClass, CommonModule],
  templateUrl: './import-teachers.component.html',
  styleUrl: './import-teachers.component.css'
})
export class ImportTeachersComponent implements OnInit{
  preferencesInterface : Preferences | undefined;

  darkTheme = signal(false);
  files: File[] = [];
  public btnDisable = signal(false); 

  loginForm = this.form.group({
    Correo : ['',
      [Validators.required, Validators.pattern(/^[a-zA-Z0-9_%+-][a-zA-Z0-9._%+-]*@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)]],
  });

  constructor(
    private form : FormBuilder,
    private auth : AuthService,
    private router : Router,
    private alerts : AlertsService,
    private preferences : PreferencesService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferencesInterface : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  disableBtn(){
    this.btnDisable.set(true);

    setTimeout(() => {
      this.btnDisable.set(false);
    }, 3000);
  }

  onFileChange(event: any): void {
    const selectedFiles = event.target.files;
  
    for (let i = 0; i < 1; i++) {
      this.files.push(selectedFiles[i]);
    }
  }

}