import { Component, OnInit, signal } from '@angular/core';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Router } from '@angular/router';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-subject-card',
  standalone: true,
  imports: [NgClass],
  templateUrl: './subject-card.component.html',
  styleUrl: './subject-card.component.css'
})
export class SubjectCardComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  openGroup(){
    this.router.navigate(['/docentes/detalles-grupo']).then(() => {
      window.location.reload();
    });
  }

}