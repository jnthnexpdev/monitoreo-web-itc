import { NgClass } from '@angular/common';
import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../auth/services/auth/auth.service';
import { Router } from '@angular/router';
import { AlertsService } from '../../../shared/services/alerts/alerts.service';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';

@Component({
  selector: 'app-register-teacher',
  standalone: true,
  imports: [NgClass],
  templateUrl: './register-teacher.component.html',
  styleUrl: './register-teacher.component.css'
})
export class RegisterTeacherComponent implements OnInit{
  preferencesInterface : Preferences | undefined;

  darkTheme = signal(false);

  public btnDisable = signal(false); 
  public inputEmail = signal(true);

  loginForm = this.form.group({
    Correo : ['',
      [Validators.required, Validators.pattern(/^[a-zA-Z0-9_%+-][a-zA-Z0-9._%+-]*@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)]],
  });

  constructor(
    private form : FormBuilder,
    private auth : AuthService,
    private router : Router,
    private alerts : AlertsService,
    private preferences : PreferencesService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferencesInterface : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  disableBtn(){
    this.btnDisable.set(true);

    setTimeout(() => {
      this.btnDisable.set(false);
    }, 3000);
  }

}