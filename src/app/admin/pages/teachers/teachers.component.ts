import { Component, OnInit, signal } from '@angular/core';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Router } from '@angular/router';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { NgClass } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { RegisterTeacherComponent } from '../../components/register-teacher/register-teacher.component';
import { ImportTeachersComponent } from '../../components/import-teachers/import-teachers.component';

@Component({
  selector: 'app-teachers',
  standalone: true,
  imports: [NgClass],
  templateUrl: './teachers.component.html',
  styleUrl: './teachers.component.css'
})
export class TeachersComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router,
    private dialog : MatDialog
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences() : void{
    this.darkTheme.set(this.preferences.getThemeState());
  }
  
  addGroup() : void{
    setTimeout(() => {
      this.router.navigate(['/docentes/agregar-grupo']);
    },1);
  }

  addTeacher() : void{
    this.dialog.open(RegisterTeacherComponent);
  }

  importTeachers() : void{
    this.dialog.open(ImportTeachersComponent)
  }

}