import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NgClass } from '@angular/common';
import { SubjectCardComponent } from "../../components/subject-card/subject-card.component";
import { ImportTeachersComponent } from '../../components/import-teachers/import-teachers.component';

@Component({
    selector: 'app-subjects',
    standalone: true,
    templateUrl: './subjects.component.html',
    styleUrl: './subjects.component.css',
    imports: [NgClass, SubjectCardComponent]
})
export class SubjectsComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router,
    private dialog : MatDialog
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences() : void{
    this.darkTheme.set(this.preferences.getThemeState());
  }
  
  addGroup() : void{
    setTimeout(() => {
      this.router.navigate(['/docentes/agregar-grupo']);
    },1);
  }

  importData() : void{
    this.dialog.open(ImportTeachersComponent)
  }

}