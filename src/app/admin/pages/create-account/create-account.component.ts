import { NgClass } from '@angular/common';
import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { AlertsService } from '../../../shared/services/alerts/alerts.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/services/auth/auth.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-account',
  standalone: true,
  imports: [NgClass],
  templateUrl: './create-account.component.html',
  styleUrl: './create-account.component.css'
})
export class CreateAccountComponent implements OnInit{
  preferencesInterface : Preferences | undefined;

  darkTheme = signal(false);
  isAdmin = signal(false);
  isTeacher = signal(false);

  public btnDisable = signal(false); 
  public inputEmail = signal(true);
  public inputPassword = signal(true);

  loginForm = this.form.group({
    Correo : ['',
      [Validators.required, Validators.pattern(/^[a-zA-Z0-9_%+-][a-zA-Z0-9._%+-]*@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)]],
    Password : ['',
      [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-záéíóúñ])(?=.*[A-ZÁÉÍÓÚÑ])[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]{8,25}$/)]]
  });

  constructor(
    private form : FormBuilder,
    private auth : AuthService,
    private router : Router,
    private alerts : AlertsService,
    private preferences : PreferencesService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferencesInterface : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  disableBtn(){
    this.btnDisable.set(true);

    setTimeout(() => {
      this.btnDisable.set(false);
    }, 3000);
  }

}