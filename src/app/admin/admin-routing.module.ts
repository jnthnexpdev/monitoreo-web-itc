import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { authGuard } from '../auth/guards/auth.guard';
import { adminGuard } from '../auth/guards/admin.guard';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { SubjectsComponent } from './pages/subjects/subjects.component';
import { TeachersComponent } from './pages/teachers/teachers.component';

const routes: Routes = [
  {
    path : 'registrar-cuenta',
    component : CreateAccountComponent,
    canActivate : [authGuard, adminGuard]
  },
  {
    path : 'materias',
    component : SubjectsComponent,
    canActivate : [authGuard, adminGuard]
  },
  {
    path : 'lista-docentes',
    component : TeachersComponent,
    canActivate : [authGuard, adminGuard]
  },
  {
    path : '',
    redirectTo : 'dashboard',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
