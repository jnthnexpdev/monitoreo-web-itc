import { Component, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

import { MenuComponent } from "./shared/components/menu/menu.component";
import { FooterComponent } from "./shared/components/footer/footer.component";
import { Preferences } from './shared/interfaces/preferences.interface';
import { PreferencesService } from './shared/services/preferences/preferences.service';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    imports: [CommonModule, RouterOutlet, MenuComponent, FooterComponent]
})
export class AppComponent implements OnInit{
  title = 'Calificaciones';
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

}