import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { AlertsService } from '../../../shared/services/alerts/alerts.service';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [NgClass, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{
  preferencesInterface : Preferences | undefined;

  darkTheme = signal(false);
  isAdmin = signal(false);
  isTeacher = signal(false);

  public btnDisable = signal(false); 
  public inputEmail = signal(true);
  public inputPassword = signal(true);
  public emailText: string = 'ejemplo@email.com';
  public passwordText: string = '********';

  loginForm = this.form.group({
    Correo : ['',
      [Validators.required, Validators.pattern(/^[a-zA-Z0-9_%+-][a-zA-Z0-9._%+-]*@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)]],
    Password : ['',
      [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-záéíóúñ])(?=.*[A-ZÁÉÍÓÚÑ])[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]{8,25}$/)]]
  });

  constructor(
    private form : FormBuilder,
    private auth : AuthService,
    private router : Router,
    private alerts : AlertsService,
    private preferences : PreferencesService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferencesInterface : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  disableBtn(){
    this.btnDisable.set(true);

    setTimeout(() => {
      this.btnDisable.set(false);
    }, 3000);
  }

  validateForm(){
    this.disableBtn();
    
    if(this.loginForm.valid){
      this.loginUser();
      this.redirectUser();
    }else{
      Object.keys(this.loginForm.controls).forEach(key => {
        const control = this.loginForm.get(key);

        if(control?.invalid){
          if(key === 'Correo'){
            this.inputEmail.set(false);
          }else if(key === 'Password'){
            this.inputPassword.set(false);
          }

          setTimeout(() => {
            this.inputEmail.set(true);
            this.inputPassword.set(true);
          }, 3000);
        }
      });
    }
  }

  loginUser(){
    const formData = this.loginForm.value;

      this.auth.loginUser(formData).subscribe((data : any) => {
        if(data.success === true){
          this.alerts.successAlert('Sesion iniciada');
        }
      }, (error : any) => {
        this.alerts.errorAlert(error.error.message);
      });
  }

  redirectUser(){
    setTimeout(() => {

      this.auth.getUserType().subscribe(userType => {
        if(userType === 'Administrador'){
          this.router.navigate(['/administrador/lista-docentes']).then(() => {
            window.location.reload();
          });
        }else if(userType === 'Docente'){
          this.router.navigate(['/docentes/ver-grupos']).then(() => {
            window.location.reload();
          });
        }
      });

    }, 3001);
  }

}