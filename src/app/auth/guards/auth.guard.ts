import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { map } from 'rxjs';

export const authGuard: CanActivateFn = (route, state) => {
  const auth = inject(AuthService);
  const router = inject(Router);

  return auth.isUserLogged().pipe(
    map(isLogged => {
      
      if(isLogged === true){
        return true;
      }else{
        setTimeout(() => {
          router.navigate(['/cuenta/iniciar-sesion']).then(() => {
            window.location.reload();
          });
        }, 1);
        return false;
      }
      
    })
  );

};