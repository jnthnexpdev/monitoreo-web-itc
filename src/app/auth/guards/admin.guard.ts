import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { map } from 'rxjs';

export const adminGuard: CanActivateFn = (route, state) => {
  
  const auth = inject(AuthService);

  return auth.getUserType().pipe(
    map(userType => {
      
      if(userType === 'Administrador'){
        return true;
      }else{
        return false;
      }

    })
  );

};