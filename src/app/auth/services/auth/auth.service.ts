import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookiesService } from '../cookies/cookies.service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment.development';
import { Observable, map } from 'rxjs';
import { User } from '../../../shared/interfaces/user.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http : HttpClient,
    private cookies : CookiesService,
    private router : Router
  ){}

  loginUser(body : any){
    const options = { withCredentials: true };
    return this.http.post(`${environment.api}/auth/login`, body, options);
  }

  registerAdmin(body : any){
    const options = { withCredentials: true };
    return this.http.post(`${environment.api}/admin/create-account`, body, options);
  }

  registerTeacher(body : any){
    const options = { withCredentials: true };
    return this.http.post(`${environment.api}/teacher/create-account`, body, options);
  }

  async logoutUser(){
    this.cookies.removeCookie('dataSession');
    
    setTimeout(() => {
      this.router.navigate(['/cuenta/iniciar-sesion']).then(() => {
        window.location.reload();
      });
    }, 1);
  }

  isUserLogged(): Observable<boolean>{
    const options = { withCredentials: true };
    return this.http.get<{success: boolean, message : string}>(`${environment.api}/auth/user-logged`, options)
    .pipe(map(response => response.success));
  }

  getUserType(): Observable<string>{
    const options = { withCredentials: true };
    return this.http.get<{account : string}>(`${environment.api}/auth/user-type`, options)
    .pipe(map(response => response.account));
  }

  getUserInfo(): Observable<User>{
    const options = { withCredentials: true };
    return this.http.get<User>(`${environment.api}/auth/user-info`, options);
  }
  
}