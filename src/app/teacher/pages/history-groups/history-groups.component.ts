import { NgClass } from '@angular/common';
import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history-groups',
  standalone: true,
  imports: [NgClass],
  templateUrl: './history-groups.component.html',
  styleUrls: ['./history-groups.component.css', './history-responsive.css']
})
export class HistoryGroupsComponent implements OnInit{

  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

}