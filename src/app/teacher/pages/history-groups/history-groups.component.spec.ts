import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryGroupsComponent } from './history-groups.component';

describe('HistoryGroupsComponent', () => {
  let component: HistoryGroupsComponent;
  let fixture: ComponentFixture<HistoryGroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HistoryGroupsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HistoryGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
