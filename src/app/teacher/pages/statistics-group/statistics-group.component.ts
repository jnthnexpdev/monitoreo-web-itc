import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Router } from '@angular/router';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-statistics-group',
  standalone: true,
  imports: [NgClass],
  templateUrl: './statistics-group.component.html',
  styleUrls: ['./statistics-group.component.css', './statistics-responsive.css']
})
export class StatisticsGroupComponent implements OnInit{

  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

}