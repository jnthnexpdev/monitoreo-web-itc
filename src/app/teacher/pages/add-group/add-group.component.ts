import { CommonModule, NgClass } from '@angular/common';
import { Component, OnInit, signal } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { Router } from '@angular/router';

import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';

@Component({
  selector: 'app-add-group',
  standalone: true,
  imports: [ 
    NgClass,
    MatStepperModule,
    MatIconModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    CommonModule
  ],
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css', './add-responsive.css', './dark-theme.css']
})
export class AddGroupComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;
  isLinear: boolean = false;
  files: File[] = [];

  groupForm = this.form.group({
    Grupo : ['', Validators.required],
    Materia : ['', Validators.required],
    Periodo : ['', Validators.required]
  });

  unitForm = this.form.group({
    units : this.form.array([]),
  });

  constructor(
    private preferences : PreferencesService,
    private router : Router,
    private form : FormBuilder
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
    this.addInitialUnits();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  backGroups(){
    this.router.navigate(['/docentes/mis-grupos']);
  }

  addInitialUnits(){
    const unitsArray = this.unitForm.get('units') as FormArray;
  
    for (let i = 0; i < 3; i++) {
      const unitGroup = this.createUnit();
      unitsArray.push(unitGroup);
    }
  }

  createUnit(){
    return this.form.group({
      Actividades: this.form.array([])
    });
  }

  createActivity(){
    return this.form.group({
      Actividad : ['', Validators.required],
      Porcentaje : ['', Validators.required]
    });
  }

  get unitControls() {
    const unitsArray = this.unitForm.get('units') as FormArray;
    return unitsArray instanceof FormArray ? unitsArray.controls : [];
  }

  getActivityControls(unitIndex: number) {
    const unitArray = this.unitForm.get('units') as FormArray;
    return (unitArray.at(unitIndex).get('Actividades') as FormArray).controls;
  }

  addUnit(){
    const unitsArray = this.unitForm.get('units') as FormArray;
    unitsArray.push(this.createUnit());
  }

  addActivity(unit : number){
    const unitArray = this.unitForm.get('units') as FormArray;
    const activitiesArray = (unitArray.at(unit).get('Actividades') as FormArray);
    activitiesArray.push(this.createActivity());
  }

  removeUnit() {
    const unitsArray = this.unitForm.get('units') as FormArray;
    if (unitsArray.length > 1) {
      unitsArray.removeAt(unitsArray.length - 1);
    }
    console.log('Unidades: ',unitsArray.length);
  }
  
  removeActivity(unit : number){
    const activitiesArray = (this.unitControls[unit].get('Actividades') as FormArray);
    if(activitiesArray.length > 1){
      activitiesArray.removeAt(activitiesArray.length - 1);
    }
    console.log('Actividades: ',activitiesArray.length);
  }

  onFileChange(event: any): void {
    const selectedFiles = event.target.files;
  
    for (let i = 0; i < 1; i++) {
      this.files.push(selectedFiles[i]);
    }
  }

}