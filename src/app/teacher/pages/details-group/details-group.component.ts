import { NgClass } from '@angular/common';
import { Component, OnInit, signal } from '@angular/core';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { Router } from '@angular/router';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';

@Component({
  selector: 'app-details-group',
  standalone: true,
  imports: [NgClass],
  templateUrl: './details-group.component.html',
  styleUrl: './details-group.component.css'
})
export class DetailsGroupComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

}