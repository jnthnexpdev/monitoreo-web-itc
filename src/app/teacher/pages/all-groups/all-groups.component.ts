import { Component, OnInit, signal } from '@angular/core';
import { CardGroupComponent } from "../../components/card-group/card-group.component";
import { NgClass } from '@angular/common';
import { Router } from '@angular/router';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Preferences } from '../../../shared/interfaces/preferences.interface';

@Component({
    selector: 'app-all-groups',
    standalone: true,
    templateUrl: './all-groups.component.html',
    styleUrl: './all-groups.component.css',
    imports: [CardGroupComponent, NgClass]
})
export class AllGroupsComponent implements OnInit{
    darkTheme = signal(false);
    preferencesInterface : Preferences | undefined;
  
    constructor(
      private preferences : PreferencesService,
      private router : Router
    ){}
  
    ngOnInit(): void {
      this.preferences.preferences$.subscribe((preferences : Preferences) => {
        this.getPreferences();
      });
    
      this.getPreferences();
    }
  
    getPreferences(){
      this.darkTheme.set(this.preferences.getThemeState());
    }
    
    addGroup(){
      setTimeout(() => {
        this.router.navigate(['/docentes/agregar-grupo']);
      },1);
    }
  
  }