import { Component, OnInit, signal } from '@angular/core';
import { Router } from '@angular/router';
import { PreferencesService } from '../../../shared/services/preferences/preferences.service';
import { Preferences } from '../../../shared/interfaces/preferences.interface';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-card-group',
  standalone: true,
  imports: [NgClass],
  templateUrl: './card-group.component.html',
  styleUrl: './card-group.component.css'
})
export class CardGroupComponent implements OnInit{
  darkTheme = signal(false);
  preferencesInterface : Preferences | undefined;

  constructor(
    private preferences : PreferencesService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
  
    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  openGroup(){
    this.router.navigate(['/docentes/detalles-grupo']).then(() => {
      window.location.reload();
    });
  }

}