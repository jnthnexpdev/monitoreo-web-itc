import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllGroupsComponent } from './pages/all-groups/all-groups.component';
import { DetailsGroupComponent } from './pages/details-group/details-group.component';
import { AddGroupComponent } from './pages/add-group/add-group.component';
import { StatisticsGroupComponent } from './pages/statistics-group/statistics-group.component';
import { HistoryGroupsComponent } from './pages/history-groups/history-groups.component';
import { authGuard } from '../auth/guards/auth.guard';
import { teacherGuard } from '../auth/guards/teacher.guard';

const routes: Routes = [
  {
    path : 'ver-grupos',
    component : AllGroupsComponent,
    canActivate : [authGuard, teacherGuard]
  },
  {
    path : 'detalles-grupo',
    component : DetailsGroupComponent,
    canActivate : [authGuard, teacherGuard]
  },
  {
    path : 'agregar-grupo',
    component : AddGroupComponent,
    canActivate : [authGuard, teacherGuard]
  },
  {
    path : 'estadisticas',
    component : StatisticsGroupComponent,
    canActivate : [authGuard, teacherGuard]
  },
  {
    path : 'historial-grupos',
    component : HistoryGroupsComponent,
    canActivate : [authGuard, teacherGuard]
  },
  {
    path : '**',
    redirectTo : 'ver-grupos',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
