import { Routes } from '@angular/router';
import { authGuard } from './auth/guards/auth.guard';
import { teacherGuard } from './auth/guards/teacher.guard';
import { adminGuard } from './auth/guards/admin.guard';

export const routes: Routes = [
    {
        path : 'cuenta',
        loadChildren : () => import('./auth/auth.module').then(m => m.AuthModule)
    },
    {
        path : 'docentes',
        loadChildren : () => import('./teacher/teacher.module').then(m => m.TeacherModule),
    },
    {
        path : 'administrador',
        loadChildren : () => import('./admin/admin.module').then(m => m.AdminModule),
    },
    {
        path : '**',
        redirectTo : 'cuenta',
        pathMatch : 'full'
    },
];