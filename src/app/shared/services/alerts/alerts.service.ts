import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorAlertComponent } from '../../components/error-alert/error-alert.component';
import { SuccessAlertComponent } from '../../components/success-alert/success-alert.component';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(private dialog : MatDialog){}

  private openAlert<T>(component : ComponentType<T>, data : any, duration : number = 3000): MatDialogRef<T>{
    const dialogRef : MatDialogRef<T> = this.dialog.open(component, {
      data : data
    });

    dialogRef.afterOpened().subscribe(() => {
      setTimeout(() => {
        dialogRef.close();
      }, duration);
    });

    return dialogRef;
  }

  errorAlert(message : string, duration : number = 3000): MatDialogRef<ErrorAlertComponent>{
    return this.openAlert(ErrorAlertComponent, { message }, duration);
  }

  successAlert(message : string, duration : number = 3000): MatDialogRef<SuccessAlertComponent>{
    return this.openAlert(SuccessAlertComponent, { message }, duration);
  }

}