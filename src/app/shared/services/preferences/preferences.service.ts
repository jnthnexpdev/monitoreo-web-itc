import { Injectable } from '@angular/core';
import { Preferences } from '../../interfaces/preferences.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreferencesService {

  private preferencesApp = new BehaviorSubject<Preferences>(this.loadPreferences());
  preferences$ = this.preferencesApp.asObservable();

  constructor(){}

  getThemeState(){
    return this.preferencesApp.value.darkTheme;
  }

  toggleTheme(){
    const newPreferences = { ...this.preferencesApp.value, darkTheme : !this.preferencesApp.value.darkTheme };
    this.updatePreferences(newPreferences);
  }

  public updatePreferences(newPreferences : Preferences){
    this.preferencesApp.next(newPreferences);
    localStorage.setItem('preferences', JSON.stringify(newPreferences));
  }

  public loadPreferences():Preferences {
    const preferencesStorage = localStorage.getItem('preferences');

    if(preferencesStorage){
      const savedPreferences = JSON.parse(preferencesStorage);

      return { darkTheme : savedPreferences.darkTheme || false }
    }else{
      return { darkTheme : false }
    }
  }

}