import { Component, OnInit, signal } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { User } from '../../interfaces/user.interfaces';
import { AuthService } from '../../../auth/services/auth/auth.service';
import { PreferencesService } from '../../services/preferences/preferences.service';
import { Preferences } from '../../interfaces/preferences.interface';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent implements OnInit{
  
  userData : User | undefined;
  menuActive = signal(false);
  darkTheme = signal(false);
  isAdmin = signal(false);
  isTeacher = signal(false);
  isAuth = signal(false);

  constructor(
    private preferences : PreferencesService,
    private auth : AuthService
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });
    this.getPreferences();

    this.auth.getUserType().subscribe(userType => {
      if(userType === 'Administrador'){
        this.isAdmin.set(true);
      }else if(userType === 'Docente'){
        this.isTeacher.set(true);
      }
    });

    this.auth.isUserLogged().subscribe(isLogged => {
      if(isLogged === true){
        this.isAuth.set(true);
      }
    });

    this.getDataUser();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }

  getDataUser(){
    this.auth.getUserInfo().subscribe(
      (response : any) => {
        if(response.success === true){
          this.userData = response.user as User;
        }else{
          this.auth.logoutUser();
        }
      }
    );
  }

  toggleTheme(){
    this.preferences.toggleTheme();
    this.darkTheme.set(this.preferences.getThemeState());
  }

  logOut(){
    this.auth.logoutUser();
  }

}