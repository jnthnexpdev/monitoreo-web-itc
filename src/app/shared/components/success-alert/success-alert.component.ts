import { Component, Inject, OnInit, signal } from '@angular/core';
import { PreferencesService } from '../../services/preferences/preferences.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Preferences } from '../../interfaces/preferences.interface';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-success-alert',
  standalone: true,
  imports: [NgClass],
  templateUrl: './success-alert.component.html',
  styleUrl: './success-alert.component.css'
})
export class SuccessAlertComponent implements OnInit{
  darkTheme = signal(false);

  constructor(
    private preferences : PreferencesService,
    @Inject(MAT_DIALOG_DATA) public data : any,
  ){}

  ngOnInit(): void {
    this.preferences.preferences$.subscribe((preferences : Preferences) => {
      this.getPreferences();
    });

    this.getPreferences();
  }

  getPreferences(){
    this.darkTheme.set(this.preferences.getThemeState());
  }
  
}