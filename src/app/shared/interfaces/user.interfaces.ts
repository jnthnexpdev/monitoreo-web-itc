export interface User{
    _id : string,
    Nombre : string,
    Apellidos : string,
    Correo : string,
    Cuenta : string,
    Fecha_Registro : string,
    Hora_Registro : string
}